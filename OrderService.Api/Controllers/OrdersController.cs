﻿using Microsoft.AspNetCore.Mvc;
using OrderService.Core.Models;
using OrderService.Core.Services.Base;
using OrderService.Models;
using PromotionService.Domain.Models.DTO;

namespace OrderService.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    public class OrdersController : Controller
    {
        private readonly IOrderService orderService;
        public OrdersController(IOrderService orderService)
        {
            this.orderService = orderService;
        }

        [HttpPost]
        public ApplyAsyncModel CreateAsync([FromBody] PlacePromotionModel model)
        {
            return orderService.CreateAsync(model);
        }

        [HttpPost]
        public ApplyModel CreateSync([FromBody] PlacePromotionModel model)
        {
            return orderService.CreateSync(model);
        }

        [HttpPost]
        public ApplyModel GetStatus([FromBody] GetStatusModel model)
        {
            return orderService.GetStatus(model);
        }
    }
}
