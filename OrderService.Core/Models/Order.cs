﻿using PromotionService.Domain.Models;

using System;

namespace OrderService.Core.Models
{
    public class Order
    {
        public Order()
        {

        }

        public Order(long customerId, string promotionId) {
            Created = DateTime.UtcNow;
            Status = OrderStatus.Pending;
            CustomerId = customerId;
            PromotionId = promotionId;
        }

        public int Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Completed { get; set; }
        public OrderStatus Status { get; set; }
        public long CustomerId { get; set; }
        public string PromotionId { get; set; }

        public void Complete()
        {
            if (Status == OrderStatus.Pending)
            {
                Status = OrderStatus.Success;
                Completed = DateTime.UtcNow;
            }
        }
        public void Fail()
        {
            if (Status == OrderStatus.Pending)
            {
                Status = OrderStatus.Fail;
                Completed = DateTime.UtcNow;
            }
        }
    }
}
