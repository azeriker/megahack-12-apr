﻿namespace PromotionService.Domain.Models.DTO
{
    public class GetStatusModel
    {
        public int DealerId { get; set; }

        public int OrderId { get; set; }
    }
}
