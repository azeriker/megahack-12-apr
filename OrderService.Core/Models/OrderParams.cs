﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrderService.Core.Models
{
    public class OrderParams
    {
        private OrderParams() { }
        public OrderParams(long customerId, string promotionId)
        {
            CustomerId = customerId;
            PromotionId = promotionId;
        }

        public long CustomerId { get; }
        public string PromotionId { get; }
    }
}
