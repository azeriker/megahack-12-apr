﻿using PromotionService.Domain.Models;

namespace OrderService.Core.Models
{
    public class ApplyModel
    {
        public int OrderId { get; set; }

        public OrderStatus OrderStatus { get; set; }
    }
}
