﻿namespace OrderService.Core.Models
{
    public class SuccessRequest
    {
        public long ClientMSISDN { get; set; }

        public string PromotionId { get; set; }
    }
}
