﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OrderService.Core.Models;
using OrderService.Core.Services.Base;
using OrderService.Models;
using PromotionService.Domain.Models;
using PromotionService.Domain.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace OrderService.Core.Services
{
    public class OrderService : IOrderService
    {
        private readonly string customerStorageEndpoint;
        private readonly IOrderStorage orderStorage;

        public OrderService(
            IConfiguration configuration,
            IOrderStorage orderStorage)
        {
            customerStorageEndpoint = configuration.GetSection("CustomerStorageEndpoint").Value;
            this.orderStorage = orderStorage;
        }

        public ApplyAsyncModel CreateAsync(PlacePromotionModel model)
        {
            var order = new Order(model.MSISDN, model.PromotionId);
            var orderId = orderStorage.Add(order);
            return new ApplyAsyncModel { OrderId = orderId };
        }

        public ApplyModel CreateSync(PlacePromotionModel model)
        {
            var order = new Order(model.MSISDN, model.PromotionId);
            var orderId = orderStorage.Add(order);
            return new ApplyModel { OrderId = orderId, OrderStatus = order.Status };
        }

        public ApplyModel GetStatus(GetStatusModel model)
        {
            var order = Get(model.OrderId);
            return new ApplyModel { OrderId = order.Id, OrderStatus = order.Status };
        }

        public Order Get(int id)
        {
            return orderStorage.Get(id);
        }

        public IList<Order> Get()
        {
            return orderStorage.Get();
        }

        public void Process()
        {
            var orders = Get();
            var pendings = orders.Where(o => o.Status == OrderStatus.Pending);

            var httpClient = new HttpClient { BaseAddress = new Uri(customerStorageEndpoint) };
            foreach(var pending in pendings)
            {
                var successRequest = new SuccessRequest
                {
                    ClientMSISDN = pending.CustomerId,
                    PromotionId = pending.PromotionId
                };

                var content = new StringContent(JsonConvert.SerializeObject(successRequest), Encoding.UTF8, "application/json");
                var response = httpClient.PostAsync($"api/Order/Success", content).Result;
                if (response.IsSuccessStatusCode)
                    pending.Complete();
                else
                    pending.Fail();

                orderStorage.Update(pending);
            }
        }
    }
}
