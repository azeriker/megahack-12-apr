﻿using OrderService.Core.Models;
using OrderService.Models;
using PromotionService.Domain.Models.DTO;
using System.Collections.Generic;

namespace OrderService.Core.Services.Base
{
    public interface IOrderService
    {
        ApplyModel CreateSync(PlacePromotionModel model);
        ApplyAsyncModel CreateAsync(PlacePromotionModel model);
        ApplyModel GetStatus(GetStatusModel model);
        void Process();
        Order Get(int id);

        IList<Order> Get();
    }
}
