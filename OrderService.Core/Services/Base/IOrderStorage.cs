﻿using OrderService.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrderService.Core.Services.Base
{
    public interface IOrderStorage
    {
        int Add(Order order);
        Order Get(int id);
        void Update(Order order);

        IList<Order> Get();
    }
}
