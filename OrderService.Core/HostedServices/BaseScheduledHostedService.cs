﻿using Microsoft.Extensions.Hosting;

using System;
using System.Threading;
using System.Threading.Tasks;

namespace OrderService.Core.HostedServices
{
    public abstract class BaseSchedulerHostedService : IHostedService, IDisposable
    {
        private Timer _timer;

        protected BaseSchedulerHostedService()
        {
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(BackgroundWork, null, TimeSpan.Zero, GetTime());
            return Task.CompletedTask;
        }


        private void BackgroundWork(object state)
        {
            if (!IsHostedServicesEnable) return;
            string thisClassName = String.Empty;

            try
            {
                thisClassName = this.GetType().Name;
                Process();
            }
            catch (Exception ex)
            {
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }

        protected bool IsHostedServicesEnable
        {
            get
            {
                return true;
            }
        }

        protected abstract TimeSpan GetTime();
        protected abstract void Process();
    }
}
