﻿using Microsoft.Extensions.DependencyInjection;
using OrderService.Core.Services.Base;

using System;

namespace OrderService.Core.HostedServices
{
    public class OrderScheduledHostedService : BaseSchedulerHostedService
    {
        private readonly IServiceScopeFactory serviceScopeFactory;

        public OrderScheduledHostedService(IServiceScopeFactory serviceScopeFactory) : base()
        {
            this.serviceScopeFactory = serviceScopeFactory;
        }

        protected override TimeSpan GetTime()
        {
            return TimeSpan.FromMinutes(1);
        }

        protected override void Process()
        {
            using (var scope = serviceScopeFactory.CreateScope())
            {
                var orderService = scope.ServiceProvider.GetService<IOrderService>();
                orderService.Process();
            }
        }
    }
}
