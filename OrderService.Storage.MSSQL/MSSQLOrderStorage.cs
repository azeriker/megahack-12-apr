﻿using OrderService.Core.Models;
using OrderService.Core.Services.Base;
using System.Collections.Generic;
using System.Linq;

namespace OrderService.Storage.MSSQL
{
    public class MSSQLOrderStorage : IOrderStorage
    {
        private readonly MsSqlContext context;

        public MSSQLOrderStorage(MsSqlContext context)
        {
            this.context = context;
        }

        public int Add(Order order)
        {
            context.Orders.Add(order);
            context.SaveChanges();
            return order.Id;
        }

        public Order Get(int id)
        {
            return context.Orders.Find(id);
        }

        public IList<Order> Get()
        {
            return context.Orders.ToList();
        }

        public void Update(Order order)
        {
            context.Orders.Update(order);
            context.SaveChanges();
        }
    }
}
