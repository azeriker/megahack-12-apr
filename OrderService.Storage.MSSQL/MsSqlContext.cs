﻿using Microsoft.EntityFrameworkCore;

using OrderService.Core.Models;

namespace OrderService.Storage.MSSQL
{
    public class MsSqlContext : DbContext
    {
        public DbSet<Order> Orders { get; set; }

        public MsSqlContext(DbContextOptions<MsSqlContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
