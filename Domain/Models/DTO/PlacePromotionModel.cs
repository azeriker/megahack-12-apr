﻿namespace PromotionService.Domain.Models.DTO
{
    public class PlacePromotionModel
    {
        public long MSISDN { get; set; }

        public int DealerId { get; set; }

        public string PromotionId { get; set; }
    }
}
