﻿namespace PromotionService.Domain.Models.DTO
{
    public class SuccessModel
    {
        public long ClientMSISDN { get; set; }

        public string PromotionId { get; set; }
    }
}
