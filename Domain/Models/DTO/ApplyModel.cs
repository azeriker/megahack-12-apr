﻿namespace PromotionService.Domain.Models.DTO
{
    public class ApplyModel
    {
        public int OrderId { get; set; }

        public OrderStatus OrderStatus { get; set; }
    }
}
