﻿using System.Collections;
using System.Collections.Generic;
using PromotionService.Domain.Models;
using PromotionService.Domain.Repositories.Base;
using PromotionService.Domain.Services.Base;

namespace PromotionService.Domain.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerService(ICustomerRepository customerRepository)
        {
            this._customerRepository = customerRepository;
        }

        public void Add(IEnumerable<Customer> customers)
        {
            _customerRepository.Add(customers);
        }

        public Customer Get(string id)
        {
            return _customerRepository.Get(id);
        }

        public Customer GetByMSISDN(long MSISDN)
        {
            return _customerRepository.GetByMSISDN(MSISDN);
        }

        public void Update(Customer customer)
        {
            _customerRepository.Update(customer);
        }
    }
}
