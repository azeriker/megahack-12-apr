﻿using System.Collections.Generic;

using PromotionService.Domain.Models;
using PromotionService.Domain.Models.DTO;

namespace PromotionService.Domain.Services.Base
{
    public interface IPromotionService
    {
        ApplyAsyncModel ApplyAsync(PlacePromotionModel placePromotionModel);

        ApplyModel Apply(PlacePromotionModel placePromotionModel);

        void Add(IEnumerable<Promotion> promotions);

        ApplyModel GetStatus(GetStatusModel getStatusModel);
    }
}
