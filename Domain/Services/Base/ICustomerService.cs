﻿using System.Collections.Generic;
using PromotionService.Domain.Models;

namespace PromotionService.Domain.Services.Base
{
    public interface ICustomerService
    {
        void Add(IEnumerable<Customer> customers);

        Customer Get(string id);

        Customer GetByMSISDN(long MSISDN);

        void Update(Customer customer);
    }
}
