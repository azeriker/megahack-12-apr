﻿using PromotionService.Domain.Models.DTO;

namespace PromotionService.Domain.Services.Base
{
    public interface IOrderService
    {
        ApplyAsyncModel PlaceAsync(PlacePromotionModel placePromotionModel);

        ApplyModel Place(PlacePromotionModel placePromotionModel);

        ApplyModel GetStatus(GetStatusModel getStatusModel);
    }
}
