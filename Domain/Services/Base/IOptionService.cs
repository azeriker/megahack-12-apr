﻿using System.Collections.Generic;

using PromotionService.Domain.Models;

namespace PromotionService.Domain.Services.Base
{
    public interface IOptionService
    {
        IEnumerable<Option> Get();
    }
}
