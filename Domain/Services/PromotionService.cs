﻿using System.Collections.Generic;
using System.Linq;
using PromotionService.Domain.Models;
using PromotionService.Domain.Models.DTO;
using PromotionService.Domain.Repositories.Base;
using PromotionService.Domain.Services.Base;

namespace PromotionService.Domain.Services
{
    public class PromotionService : IPromotionService
    {
        private readonly IPromotionRepository promotionRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderService orderService;

        public PromotionService(
            IPromotionRepository promotionRepository,
            ICustomerRepository customerRepository,
            IOrderService orderService)
        {
            this.promotionRepository = promotionRepository;
            this._customerRepository = customerRepository;
            this.orderService = orderService;
        }

        public ApplyAsyncModel ApplyAsync(PlacePromotionModel placePromotionModel)
        {
            ApplyAsyncModel model = null;
            if (CheckConditions(placePromotionModel))
            {
                model = orderService.PlaceAsync(placePromotionModel);
            }

            return model;
        }

        public ApplyModel Apply(PlacePromotionModel placePromotionModel)
        {
            ApplyModel model = null;
            if (CheckConditions(placePromotionModel))
            {
                model = orderService.Place(placePromotionModel);
            }

            return model;
        }

        public void Add(IEnumerable<Promotion> promotions)
        {
            promotionRepository.Add(promotions);
        }

        public ApplyModel GetStatus(GetStatusModel getStatusModel)
        {
            return orderService.GetStatus(getStatusModel);
        }

        private bool CheckConditions(PlacePromotionModel model)
        {
            var client = _customerRepository.GetByMSISDN(model.MSISDN);
            var promotion = promotionRepository.Get(model.PromotionId);
            if (!client.Options.Any(o => promotion.Options.Contains(o)))
            {
                if (MatchConditions(client, promotion))
                {
                    return true;
                }
            }

            return false;
        }

        private bool MatchConditions(Customer customer, Promotion promotion)
        {
            foreach (var condition in promotion.GetConditions())
                if (condition != null && !condition.IsMatch(customer))
                    return false;

            return true;
        }
    }
}
