﻿using System;
using System.Net.Http;
using System.Text;
using Microsoft.Extensions.Configuration;

using Newtonsoft.Json;

using PromotionService.Domain.Models.DTO;
using PromotionService.Domain.Services.Base;

namespace PromotionService.Domain.Services
{
    public class OrderService : IOrderService
    {
        private readonly HttpClient httpClient;

        public OrderService(IConfiguration configuration)
        {
            this.httpClient = InitializeHttpClient(configuration);
        }

        public ApplyAsyncModel PlaceAsync(PlacePromotionModel placePromotionModel)
        {
            var content = new StringContent(JsonConvert.SerializeObject(placePromotionModel), Encoding.UTF8, "application/json");
            var response = httpClient.PostAsync("api/Orders/CreateAsync", content).Result.Content.ReadAsStringAsync().Result;
            var applyAsyncModel = JsonConvert.DeserializeObject<ApplyAsyncModel>(response);
            return applyAsyncModel;
        }

        public ApplyModel Place(PlacePromotionModel placePromotionModel)
        {
            var content = new StringContent(JsonConvert.SerializeObject(placePromotionModel), Encoding.UTF8, "application/json");
            var response = httpClient.PostAsync("api/Orders/CreateSync", content).Result.Content.ReadAsStringAsync().Result;
            var applyModel = JsonConvert.DeserializeObject<ApplyModel>(response);
            return applyModel;
        }

        public ApplyModel GetStatus(GetStatusModel getStatusModel)
        {
            var content = new StringContent(JsonConvert.SerializeObject(getStatusModel), Encoding.UTF8, "application/json");
            var response = httpClient.PostAsync("api/Orders/GetStatus", content).Result.Content.ReadAsStringAsync().Result;
            var status = JsonConvert.DeserializeObject<ApplyModel>(response);
            return status;
        }

        private HttpClient InitializeHttpClient(IConfiguration configuration)
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri(configuration.GetSection("OrderServiceEndpoint").Value)
            };

            return client;
        }
    }
}
