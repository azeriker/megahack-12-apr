﻿using System.Collections.Generic;
using PromotionService.Domain.Models;

namespace PromotionService.Domain.Repositories.Base
{
    public interface IPromotionRepository
    {
        void Add(IEnumerable<Promotion> promotions);

        Promotion Get(string id);
    }
}
