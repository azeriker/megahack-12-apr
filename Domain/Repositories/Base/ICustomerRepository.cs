﻿using System.Collections.Generic;
using PromotionService.Domain.Models;

namespace PromotionService.Domain.Repositories.Base
{
    public interface ICustomerRepository
    {
        void Add(IEnumerable<Customer> customers);

        Customer Get(string id);

        Customer GetByMSISDN(long MSISDN);

        void Update(Customer customer);
    }
}
