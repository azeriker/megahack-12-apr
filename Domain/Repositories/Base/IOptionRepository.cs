﻿using System.Collections.Generic;
using PromotionService.Domain.Models;

namespace PromotionService.Domain.Repositories.Base
{
    public interface IOptionRepository
    {
        void Add(IEnumerable<Option> options);
    }
}
