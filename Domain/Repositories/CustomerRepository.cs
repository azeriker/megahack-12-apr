﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Microsoft.Extensions.Configuration;

using Newtonsoft.Json;

using PromotionService.Domain.Models;
using PromotionService.Domain.Repositories.Base;

namespace PromotionService.Domain.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly HttpClient httpClient;
        private readonly string relativeApi = $"api/{nameof(Customer)}";

        public CustomerRepository(IConfiguration configuration)
        {
            this.httpClient = InitializeHttpClient(configuration);
        }

        public void Add(IEnumerable<Customer> customers)
        {
            var content = new StringContent(JsonConvert.SerializeObject(customers), Encoding.UTF8, "application/json");
            var response = httpClient.PostAsync($"{relativeApi}/Add", content).Result.Content.ReadAsStringAsync().Result;
        }

        public Customer Get(string id)
        {
            var content = httpClient.GetAsync($"{relativeApi}/Get?id={id}").Result.Content.ReadAsStringAsync().Result;
            var customer = JsonConvert.DeserializeObject<Customer>(content);
            return customer;
        }

        public Customer GetByMSISDN(long MSISDN)
        {
            var content = httpClient.GetAsync($"{relativeApi}/GetByMSISDN?MSISDN={MSISDN}").Result.Content.ReadAsStringAsync().Result;
            var customer = JsonConvert.DeserializeObject<Customer>(content);
            return customer;
        }

        public void Update(Customer customer)
        {
            var content = new StringContent(JsonConvert.SerializeObject(customer), Encoding.UTF8, "application/json");
            var response = httpClient.PostAsync($"{relativeApi}/Update", content).Result.Content.ReadAsStringAsync().Result;
        }

        private HttpClient InitializeHttpClient(IConfiguration configuration)
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri(configuration.GetSection("CustomerStorageEndpoint").Value)
            };

            return client;
        }
    }
}
