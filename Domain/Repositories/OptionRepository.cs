﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PromotionService.Domain.Models;
using PromotionService.Domain.Repositories.Base;

namespace PromotionService.Domain.Repositories
{
    public class OptionRepository : IOptionRepository
    {
        private readonly HttpClient httpClient;
        private readonly string relativeApi = $"api/{nameof(Option)}";

        public OptionRepository(IConfiguration configuration)
        {
            this.httpClient = InitializeHttpClient(configuration);
        }

        public void Add(IEnumerable<Option> options)
        {
            var content = new StringContent(JsonConvert.SerializeObject(options), Encoding.UTF8, "application/json");
            var response = httpClient.PostAsync($"{relativeApi}/Add", content).Result.Content.ReadAsStringAsync().Result;
        }

        private HttpClient InitializeHttpClient(IConfiguration configuration)
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri(configuration.GetSection("CustomerStorageEndpoint").Value)
            };

            return client;
        }
    }
}
