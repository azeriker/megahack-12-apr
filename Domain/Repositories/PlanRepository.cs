﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PromotionService.Domain.Models;
using PromotionService.Domain.Repositories.Base;

namespace PromotionService.Domain.Repositories
{
    public class PlanRepository : IPlanRepository
    {
        private readonly HttpClient httpClient;
        private readonly string relativeApi = $"api/{nameof(Plan)}";

        public PlanRepository(IConfiguration configuration)
        {
            this.httpClient = InitializeHttpClient(configuration);
        }

        public void Add(IEnumerable<Plan> plans)
        {
            var content = new StringContent(JsonConvert.SerializeObject(plans), Encoding.UTF8, "application/json");
            var response = httpClient.PostAsync($"{relativeApi}/Add", content).Result.Content.ReadAsStringAsync().Result;
        }

        public Plan Get(string id)
        {
            var content = httpClient.GetAsync($"{relativeApi}/Get?id={id}").Result.Content.ReadAsStringAsync().Result;
            var plan = JsonConvert.DeserializeObject<Plan>(content);
            return plan;
        }

        public IEnumerable<Plan> Get()
        {
            var content = httpClient.GetAsync($"{relativeApi}/GetAll").Result.Content.ReadAsStringAsync().Result;
            var plans = JsonConvert.DeserializeObject<IEnumerable<Plan>>(content);
            return plans;
        }

        private HttpClient InitializeHttpClient(IConfiguration configuration)
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri(configuration.GetSection("CustomerStorageEndpoint").Value)
            };

            return client;
        }
    }
}
