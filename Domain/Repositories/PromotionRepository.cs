﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using PromotionService.Domain.Models;
using PromotionService.Domain.Repositories.Base;

namespace PromotionService.Domain.Repositories
{
    public class PromotionRepository : IPromotionRepository
    {
        private readonly HttpClient httpClient;
        private readonly string relativeApi = $"api/{nameof(Promotion)}";

        public PromotionRepository(IConfiguration configuration)
        {
            this.httpClient = InitializeHttpClient(configuration);
        }

        public void Add(IEnumerable<Promotion> promotions)
        {
            var content = new StringContent(JsonConvert.SerializeObject(promotions), Encoding.UTF8, "application/json");
            var response = httpClient.PostAsync($"{relativeApi}/Add", content).Result.Content.ReadAsStringAsync().Result;
        }

        public Promotion Get(string id)
        {
            var content = httpClient.GetAsync($"{relativeApi}/Get?id={id}").Result.Content.ReadAsStringAsync().Result;
            var promotion = JsonConvert.DeserializeObject<Promotion>(content);
            return promotion;
        }

        private HttpClient InitializeHttpClient(IConfiguration configuration)
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri(configuration.GetSection("CustomerStorageEndpoint").Value)
            };

            return client;
        }
    }
}
