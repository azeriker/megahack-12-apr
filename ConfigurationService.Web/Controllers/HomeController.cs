﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ConfigurationService.Web.ViewModels;
using ConfigurationService.Web.Extensions;
using System.Net.Http;
using Newtonsoft.Json;
using PromotionService.Domain.Models;
using PromotionService.Domain.Models.Conditions;
using System.Text;

namespace ConfigurationService.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly HttpClient _client;
        public HomeController(HttpClient client)
        {
            _client = client;
        }
        public async Task<IActionResult> Index()
        {
            try
            {
                var plans = JsonConvert.DeserializeObject<List<Plan>>(await _client.GetStringAsync("api/Plan/GetAll"));
                var options = JsonConvert.DeserializeObject<List<Option>>(await _client.GetStringAsync("api/Option/GetAll"));
                ViewBag.Plans = plans;
                ViewBag.Options = options;
            }
            catch
            {
                ViewBag.Plans = new List<Plan>();
                ViewBag.Options = new List<Option>();
            }
            HttpContext.Session.SetObject("Plans", (object)ViewBag.Plans);
            HttpContext.Session.SetObject("Options", (object)ViewBag.Options);
            return View();
        }
        public async Task<CreatePromotionResultViewModel> Create([FromBody]CreatePromotionViewModel model)
        {
            //var plans = HttpContext.Session.GetObject<List<Plan>>("Plans");
            //var options = HttpContext.Session.GetObject<List<Option>>("Options");

            var plans = JsonConvert.DeserializeObject<List<Plan>>(await _client.GetStringAsync("api/Plan/GetAll"));
            var options = JsonConvert.DeserializeObject<List<Option>>(await _client.GetStringAsync("api/Option/GetAll"));

            Promotion promotion = null;
            try
            {
                promotion = new Promotion()
                {
                    Title = model.Title,
                    Description = model.Description,
                    Options = options.Where(o => model.OptionIds.Contains(o.Id.ToString())).ToList(),
                    PlanCondition = model.PlanConditionIsActive ? new PlanCondition() { Plans = plans.Where(o => model.PlanIds.Contains(o.Id.ToString())).ToList() } : null,
                    BalanceCondition = model.BalanceConditionIsActive ? new BalanceCondition() { Amount = model.Amount.Value, IsNeedToBeLowerThenAmount = model.IsNeedToBeLowerThenAmount } : null,
                    ConnectionDateCondition = model.DateConditionIsActive ? new ConnectionDateCondition() { Date = DateTime.Now.AddYears(-model.Years.Value).AddMonths(-model.Months.Value), IsNeedToBeEarlier = model.IsNeedToBeEarlier } : null,
                    OptionsCondition = model.OptionsConditionIsActive ? new OptionsCondition() { Options = options.Where(o => model.OptionCondIds.Contains(o.Id.ToString())).ToList() } : null
                };
            }
            catch {
                return new CreatePromotionResultViewModel()
                {
                    IsSuccess = false,
                    Message="Ошибка данных формы"
                };
            }
            if (promotion.GetConditions().Where(cond => cond != null).Count() > 0)
            {
                var result = _client.PostAsync("api/Promotion/Add", new StringContent(JsonConvert.SerializeObject(promotion), Encoding.UTF8, "application/json")).Result;
                if (result.IsSuccessStatusCode)
                    return new CreatePromotionResultViewModel()
                    {
                        IsSuccess = true
                    };
                return new CreatePromotionResultViewModel() { IsSuccess = false, Message = "Ошибка при записи в хранилище" };
            }
            else
            {
                return new CreatePromotionResultViewModel() { IsSuccess = false, Message = "Активированно меньше, чем 1 условие" };
            }

        }
    }
}
