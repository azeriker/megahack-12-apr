﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConfigurationService.Web.ViewModels
{
    public class CreatePromotionViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }

        public bool BalanceConditionIsActive { get; set; }
        public decimal? Amount { get; set; }
        public bool IsNeedToBeLowerThenAmount { get; set; }

        public bool PlanConditionIsActive { get; set; }
        public List<string> PlanIds { get; set; }

        public bool DateConditionIsActive { get; set; }
        public int? Years { get; set; }
        public int? Months { get; set; }
        public bool IsNeedToBeEarlier { get; set; }

        public bool OptionsConditionIsActive { get; set; }
        public List<string> OptionCondIds { get; set; }

        public List<string> OptionIds { get; set; }
    }
}
