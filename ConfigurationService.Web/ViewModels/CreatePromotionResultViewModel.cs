﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConfigurationService.Web.ViewModels
{
    public class CreatePromotionResultViewModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
