﻿namespace PromotionService.Domain.Models
{
    public enum OrderStatus
    {
        Pending,
        Success,
        Fail
    }
}
