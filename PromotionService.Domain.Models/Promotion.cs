﻿using System;
using System.Collections.Generic;

using PromotionService.Domain.Models.Conditions;
using PromotionService.Domain.Models.Conditions.Base;

namespace PromotionService.Domain.Models
{
    public class Promotion
    {
        public Promotion()
        {
            Id = Guid.NewGuid().ToString();
        }

        public string Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public List<Option> Options { get; set; }

        public PlanCondition PlanCondition { get; set; }

        public BalanceCondition BalanceCondition { get; set; }

        public ConnectionDateCondition ConnectionDateCondition { get; set; }

        public OptionsCondition OptionsCondition { get; set; }

        public IList<ICondition> GetConditions()
        {
            return new List<ICondition>
            {
                PlanCondition, 
                BalanceCondition,
                ConnectionDateCondition,
                OptionsCondition
            };
        }
    }
}
