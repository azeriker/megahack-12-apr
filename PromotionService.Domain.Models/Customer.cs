﻿using System;
using System.Collections.Generic;

namespace PromotionService.Domain.Models
{
    public class Customer
    {
        public Customer()
        {
            Id = Guid.NewGuid().ToString();
        }

        public string Id { get; set; }

        public long MSISDN { get; set; }

        public DateTime ConnectionDate { get; set; }

        public decimal Balance { get; set; }

        public Plan Plan { get; set; }

        public List<Option> Options { get; set; }
    }
}
