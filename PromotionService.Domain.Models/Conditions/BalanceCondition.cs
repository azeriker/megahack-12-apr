﻿using PromotionService.Domain.Models.Conditions.Base;

namespace PromotionService.Domain.Models.Conditions
{
    public class BalanceCondition : ICondition
    {
        public decimal Amount { get; set; }
        public bool IsNeedToBeLowerThenAmount { get; set; }

        public bool IsMatch(Customer customer) 
            => IsNeedToBeLowerThenAmount ? customer.Balance < Amount : customer.Balance > Amount;
    }
}
