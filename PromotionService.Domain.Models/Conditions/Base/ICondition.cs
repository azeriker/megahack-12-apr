﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PromotionService.Domain.Models.Conditions.Base
{
    public interface ICondition
    {
        bool IsMatch(Customer customer);
    }
}
