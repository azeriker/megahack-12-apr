﻿using System;

using PromotionService.Domain.Models.Conditions.Base;

namespace PromotionService.Domain.Models.Conditions
{
    public class ConnectionDateCondition : ICondition
    {
        public DateTime Date { get; set; }
        public bool IsNeedToBeEarlier { get; set; }

        public bool IsMatch(Customer customer) 
            => IsNeedToBeEarlier ? customer.ConnectionDate < Date : customer.ConnectionDate > Date;
    }
}
