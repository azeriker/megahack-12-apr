﻿using System.Collections.Generic;

using PromotionService.Domain.Models.Conditions.Base;

namespace PromotionService.Domain.Models.Conditions
{
    public class PlanCondition : ICondition
    {
        public List<Plan> Plans { get; set; }

        public bool IsMatch(Customer customer) => Plans.Contains(customer.Plan);
    }
}
