﻿using System.Collections.Generic;
using System.Linq;

using PromotionService.Domain.Models.Conditions.Base;

namespace PromotionService.Domain.Models.Conditions
{
    public class OptionsCondition : ICondition
    {
        public List<Option> Options { get; set; }

        public bool IsMatch(Customer customer) => Options.Intersect(customer.Options).Count() == customer.Options.Count;
    }
}
