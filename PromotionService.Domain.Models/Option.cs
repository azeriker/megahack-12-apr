﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace PromotionService.Domain.Models
{
    public class Option
    {
        public Option()
        {
            Id = Guid.NewGuid().ToString();
        }

        [BsonId]
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
