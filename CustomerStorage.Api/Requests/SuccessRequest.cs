﻿namespace CustomerStorage.Api.Requests
{
    public class SuccessRequest
    {
        public long ClientMSISDN { get; set; }

        public string PromotionId { get; set; }
    }
}
