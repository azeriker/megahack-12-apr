﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerStorage.Api.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using PromotionService.Domain.Models;

namespace CustomerStorage.Api.Contexts
{
    public class MongoContext
    {
        public IMongoCollection<CustomerModel> Customers => database.GetCollection<CustomerModel>("Customers");

        public IMongoCollection<PromotionModel> Promotions => database.GetCollection<PromotionModel>("Promotions");

        public IMongoCollection<Plan> Plans => database.GetCollection<Plan>("Plans");

        public IMongoCollection<Option> Options => database.GetCollection<Option>("Options");

        private readonly IMongoDatabase database;

        public MongoContext(IConfiguration configuration)
        {
            var connectionString = configuration.GetSection("MongoConnection").Value;
            var mongoUrl = new MongoUrl(connectionString);
            var client = new MongoClient(connectionString);
            database = client.GetDatabase(mongoUrl.DatabaseName);

            EnsureSeeded();
        }

        public IMongoCollection<TModel> GetCollection<TModel>(string collectionName)
        {
            return database.GetCollection<TModel>(collectionName);
        }

        private void EnsureSeeded()
        {
            if (Plans.Find(_ => true).CountDocuments() == 0)
            {
                SeedPlans();
            }

            if (Options.Find(_ => true).CountDocuments() == 0)
            {
                SeedOptions();
            }

            if (Customers.Find(_ => true).CountDocuments() == 0)
            {
                SeedCustomers();
            }
        }

        private void SeedCustomers()
        {
            var options = Options.Find(_ => true).ToList().ToArray();
            var plans = Plans.Find(_ => true).ToList().ToArray();
            var customers = new List<CustomerModel>();
            Random r = new Random((int)DateTime.Now.Ticks);
            for (long num = 9000000000;num< 9000999999;num++)
            {
                var custOpt = new List<Option>();
                var opLimit = r.Next(options.Length);
                for (int opCount =0; opCount < opLimit; opCount++)
                {
                    var op = options[r.Next(options.Length - 1)];
                    if (custOpt.FirstOrDefault(o => o.Id == op.Id) == null)
                        custOpt.Add(op);
                }
                customers.Add(new CustomerModel()
                {
                    Id = Guid.NewGuid().ToString(),
                    Balance = r.Next(0, 2000),
                    ConnectionDate = DateTime.Now.AddDays(-r.Next(0, 10000)),
                    MSISDN = num,
                    OptionIds = custOpt.Select(o => o.Id).ToList(),
                    PlanId = plans[r.Next(plans.Length - 1)].Id
                });
            }
            Customers.InsertMany(customers);
        }

        private void SeedOptions()
        {
            var options = new List<Option>
            {
                new Option{Name = "Option 1"},
                new Option{Name = "Option 2"},
                new Option{Name = "Option 3"},
                new Option{Name = "Option 4"},
                new Option{Name = "Option 5"},
            };

            Options.InsertMany(options);
        }

        private void SeedPlans()
        {
            var plans = new List<Plan>
            {
                new Plan{Name = "Plan 1"},
                new Plan{Name = "Plan 2"},
                new Plan{Name = "Plan 3"},
                new Plan{Name = "Plan 4"},
                new Plan{Name = "Plan 5"},
            };

            Plans.InsertMany(plans);
        }
    }
}
