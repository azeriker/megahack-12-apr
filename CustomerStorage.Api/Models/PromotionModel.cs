﻿using System.Collections.Generic;

using MongoDB.Bson.Serialization.Attributes;

using PromotionService.Domain.Models.Conditions;

namespace CustomerStorage.Api.Models
{
    public class PromotionModel
    {
        [BsonId]
        public string Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public List<string> OptionIds { get; set; }

        public PlanConditionModel PlanConditionModel { get; set; }

        public BalanceCondition BalanceCondition { get; set; }

        public ConnectionDateCondition ConnectionDateCondition { get; set; }

        public OptionsConditionModel OptionsConditionModel { get; set; }
    }
}
