﻿using System.Collections.Generic;

namespace CustomerStorage.Api.Models
{
    public class PlanConditionModel
    {
        public List<string> PlanIds { get; set; }
    }
}
