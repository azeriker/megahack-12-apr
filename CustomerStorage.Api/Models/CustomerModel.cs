﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace CustomerStorage.Api.Models
{
    public class CustomerModel
    {
        [BsonId]
        public string Id { get; set; }

        public long MSISDN { get; set; }

        public DateTime ConnectionDate { get; set; }

        public decimal Balance { get; set; }

        public string PlanId { get; set; }

        public List<string> OptionIds { get; set; }
    }
}
