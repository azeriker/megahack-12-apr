﻿using System.Collections.Generic;

using PromotionService.Domain.Models;

namespace CustomerStorage.Api.Models
{
    public class OptionsConditionModel
    {
        public List<string> OptionIds { get; set; }
    }
}
