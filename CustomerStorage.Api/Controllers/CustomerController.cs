﻿using System;
using System.Collections.Generic;
using System.Linq;
using CustomerStorage.Api.Contexts;
using CustomerStorage.Api.Controllers.Base;
using CustomerStorage.Api.Models;
using Microsoft.AspNetCore.Mvc;

using MongoDB.Driver;

using PromotionService.Domain.Models;

namespace CustomerStorage.Api.Controllers
{
    public class CustomerController : BaseController
    {
        private readonly MongoContext context;

        public CustomerController(MongoContext context) : base()
        {
            this.context = context;
        }

        [HttpPost]
        public void Add(Customer customer)
        {
            var customerModel = Map(customer);
            context.Customers.InsertOne(customerModel);
        }

        [HttpGet]
        public Customer Get(string id)
        {
            var customerModel = context.Customers.Find(i => i.Id == id).FirstOrDefault();
            return Map(customerModel);
        }
 
        [HttpGet]
        public Customer GetByMSISDN(long MSISDN)
        {
            var customerModel = context.Customers.Find(i => i.MSISDN == MSISDN).FirstOrDefault();
            return Map(customerModel);

            return new Customer
            {
                Balance = 100.0m,
                ConnectionDate = DateTime.Now,
                MSISDN = MSISDN,
                Options = new List<Option>(),
                Plan = new Plan()
            };
        }

        [HttpPost]
        public void Update(Customer customer)
        {
            var customerModel = Map(customer);
            var update = Builders<CustomerModel>.Update
                .Set(i => i.Balance, customerModel.Balance)
                .Set(i => i.OptionIds, customerModel.OptionIds)
                .Set(i => i.PlanId, customerModel.PlanId);
            context.Customers.UpdateOne(i => i.Id == customerModel.Id, update);
        }

        private Customer Map(CustomerModel model)
        {
            if (model != null)
            {
                var plan = context.Plans.Find(i => i.Id == model.PlanId).FirstOrDefault();
                var builder = Builders<Option>.Filter;
                var options = new List<Option>();
                if(model.OptionIds != null)
                {
                    var optionsFilter = builder.In(x => x.Id, model.OptionIds);
                    options = context.Options.Find(optionsFilter).ToList();
                }

                return new Customer
                {
                    Id = model.Id,
                    Balance = model.Balance,
                    ConnectionDate = model.ConnectionDate,
                    MSISDN = model.MSISDN,
                    Options = options,
                    Plan = plan
                };
            }

            return null;
        }

        private CustomerModel Map(Customer customer)
        {
            return new CustomerModel
            {
                Id = customer.Id,
                Balance = customer.Balance,
                ConnectionDate = customer.ConnectionDate,
                MSISDN = customer.MSISDN,
                OptionIds = customer.Options?.Select(o => o.Id).ToList(),
                PlanId = customer.Plan.Id
            };
        }
    }
}
