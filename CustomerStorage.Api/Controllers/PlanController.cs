﻿using System.Collections.Generic;
using CustomerStorage.Api.Contexts;
using CustomerStorage.Api.Controllers.Base;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using PromotionService.Domain.Models;

namespace CustomerStorage.Api.Controllers
{
    public class PlanController : BaseController
    {
        private readonly MongoContext context;

        public PlanController(MongoContext context) : base()
        {
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Plan> GetAll()
        {
            return context.Plans.Find(_ => true).ToList();
        }
    }
}
