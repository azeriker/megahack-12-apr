﻿using CustomerStorage.Api.Contexts;
using CustomerStorage.Api.Controllers.Base;
using CustomerStorage.Api.Models;
using CustomerStorage.Api.Requests;

using Microsoft.AspNetCore.Mvc;

using MongoDB.Driver;

namespace CustomerStorage.Api.Controllers
{
    public class OrderController : BaseController
    {
        private readonly MongoContext context;

        public OrderController(MongoContext context) : base()
        {
            this.context = context;
        }

        [HttpPost]
        public void Success([FromBody] SuccessRequest request)
        {
            var customerModel = context.Customers.Find(i => i.MSISDN == request.ClientMSISDN).FirstOrDefault();
            var promotionModel = context.Promotions.Find(i => i.Id == request.PromotionId).FirstOrDefault();

            customerModel.OptionIds.AddRange(promotionModel.OptionIds);

            var update = Builders<CustomerModel>.Update
                .Set(i => i.Balance, customerModel.Balance)
                .Set(i => i.OptionIds, customerModel.OptionIds)
                .Set(i => i.PlanId, customerModel.PlanId);
            context.Customers.UpdateOne(i => i.Id == customerModel.Id, update);
        }
    }
}
