﻿using System.Collections;
using System.Collections.Generic;
using CustomerStorage.Api.Contexts;
using CustomerStorage.Api.Controllers.Base;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using PromotionService.Domain.Models;

namespace CustomerStorage.Api.Controllers
{
    public class OptionController : BaseController
    {
        private readonly MongoContext context;

        public OptionController(MongoContext context) : base()
        {
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Option> GetAll()
        {
            return context.Options.Find(_ => true).ToList();
        }
    }
}
