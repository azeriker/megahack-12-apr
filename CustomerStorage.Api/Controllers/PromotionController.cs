﻿using System.Collections.Generic;
using System.Linq;

using CustomerStorage.Api.Contexts;
using CustomerStorage.Api.Controllers.Base;
using CustomerStorage.Api.Models;

using Microsoft.AspNetCore.Mvc;

using MongoDB.Driver;

using PromotionService.Domain.Models;
using PromotionService.Domain.Models.Conditions;

namespace CustomerStorage.Api.Controllers
{
    public class PromotionController : BaseController
    {
        private readonly MongoContext context;

        public PromotionController(MongoContext context) : base()
        {
            this.context = context;
        }

        [HttpPost]
        public void Add([FromBody] Promotion promotion)
        {
            var promotionModel = Map(promotion);
            context.Promotions.InsertOne(promotionModel);
        }

        [HttpGet]
        public Promotion Get(string id)
        {
            var promotionModel = context.Promotions.Find(i => i.Id == id).FirstOrDefault();
            return Map(promotionModel);
        }

        //[HttpPost]
        //public void Update(Customer customer)
        //{
        //    var customerModel = Map(customer);
        //    var update = Builders<CustomerModel>.Update
        //        .Set(i => i.Balance, customerModel.Balance)
        //        .Set(i => i.OptionIds, customerModel.OptionIds)
        //        .Set(i => i.PlanId, customerModel.PlanId);
        //    context.Customers.UpdateOne(i => i.Id == customerModel.Id, update);
        //}

        private Promotion Map(PromotionModel model)
        {
            if (model != null)
            {
                var planBuilder = Builders<Plan>.Filter;
                List<Plan> plans = null;
                PlanCondition planCond = null;
                if(model.PlanConditionModel!=null)
                {
                    if (model.PlanConditionModel.PlanIds != null)
                    {
                        var planFilter = planBuilder.In(x => x.Id, model.PlanConditionModel.PlanIds);
                        plans = context.Plans.Find(planFilter).ToList();
                        planCond = new PlanCondition
                        {
                            Plans = plans
                        };
                    }
                }

                List<Option> options = null;
                if (model.OptionIds != null)
                {
                    var optionBuilder = Builders<Option>.Filter;
                    var optionFilter = optionBuilder.In(x => x.Id, model.OptionIds);
                    options = context.Options.Find(optionFilter).ToList();
                }

                List<Option> conditionOptions = null;
                OptionsCondition optionCond = null;
                if (model.OptionsConditionModel != null)
                {
                    if (model.OptionsConditionModel.OptionIds != null)
                    {
                        var conditionOptionBuilder = Builders<Option>.Filter;
                        var conditionOptionFilter = conditionOptionBuilder.In(x => x.Id, model.OptionsConditionModel.OptionIds);
                        conditionOptions = context.Options.Find(conditionOptionFilter).ToList();
                        optionCond = new OptionsCondition
                        {
                            Options = conditionOptions
                        };
                    }
                }


                return new Promotion
                {
                    Id = model.Id,
                    Title = model.Title,
                    Description = model.Description,
                    Options = options,
                    PlanCondition = planCond,
                    BalanceCondition = model.BalanceCondition,
                    ConnectionDateCondition = model.ConnectionDateCondition,
                    OptionsCondition = optionCond
                };
            }

            return null;
        }

        private PromotionModel Map(Promotion promotion)
        {
            return new PromotionModel
            {
                Id = promotion.Id,
                Title = promotion.Title,
                Description = promotion.Description,
                OptionIds = promotion.Options.Select(o => o.Id).ToList(),
                PlanConditionModel = new PlanConditionModel
                {
                    PlanIds = promotion.PlanCondition?.Plans.Select(p => p.Id).ToList()
                },
                BalanceCondition = promotion.BalanceCondition,
                ConnectionDateCondition = promotion.ConnectionDateCondition,
                OptionsConditionModel = new OptionsConditionModel
                {
                    OptionIds = promotion.OptionsCondition?.Options.Select(o => o.Id).ToList()
                }
            };
        }

        //[HttpGet]
        //public override PromotionModel Get(int id)
        //{
        //    return Collection<>.Include(p => p.BalanceCondition)
        //        .Include(p => p.ConnectionDateCondition)
        //        .Include(p => p.PlanConditionModel)
        //        .Include(c => c.Options).FirstOrDefault(c => c.Id == id);
        //}
    }
}
