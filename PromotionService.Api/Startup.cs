﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using PromotionService.Domain.Repositories;
using PromotionService.Domain.Repositories.Base;
using PromotionService.Domain.Services;
using PromotionService.Domain.Services.Base;
using Swashbuckle.AspNetCore.Swagger;

namespace PromotionService.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Promotion Service API", Version = "v1" });
            });
            services.AddTransient<IOptionService, OptionService>();
            services.AddTransient<ICustomerService, CustomerService>();
            services.AddTransient<IPromotionService, Domain.Services.PromotionService>();
            services.AddTransient<IOrderService, OrderService>();

            services.AddTransient<IPromotionRepository, PromotionRepository>();
            services.AddTransient<ICustomerRepository, CustomerRepository>();
            services.AddTransient<IOptionRepository, OptionRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Promotion Service API");
                c.RoutePrefix = string.Empty;
            });
            app.UseMvc();
        }
    }
}
