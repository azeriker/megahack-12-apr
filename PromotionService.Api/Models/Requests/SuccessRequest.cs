﻿namespace PromotionService.Api.Models.Requests
{
    public class SuccessRequest
    {
        public long ClientMSISDN { get; set; }

        public int PromotionId { get; set; }
    }
}
