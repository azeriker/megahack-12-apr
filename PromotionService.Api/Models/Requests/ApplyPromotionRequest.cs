﻿namespace PromotionService.Api.Models.Requests
{
    public class ApplyPromotionRequest
    {
        public long MSISDN { get; set; }

        public int DealerId { get; set; }

        public string PromotionId { get; set; }
    }
}
