﻿namespace PromotionService.Api.Models.Requests
{
    public class GetStatusRequest
    {
        public int DealerId { get; set; }

        public int OrderId { get; set; }
    }
}
