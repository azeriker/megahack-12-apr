﻿using Microsoft.AspNetCore.Mvc;
using PromotionService.Api.Models;
using PromotionService.Api.Models.Requests;
using PromotionService.Domain.Models;
using PromotionService.Domain.Services.Base;

namespace PromotionService.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    public class OrderController : Controller
    {
        private readonly ICustomerService _customerService;

        public OrderController(ICustomerService customerService)
        {
            this._customerService = customerService;
        }
    }
}
