﻿using Microsoft.AspNetCore.Mvc;

using PromotionService.Api.Models.Requests;
using PromotionService.Domain.Models.DTO;
using PromotionService.Domain.Services.Base;

namespace PromotionService.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    public class DealerController : Controller
    {
        private readonly IPromotionService promotionService;

        public DealerController(IPromotionService promotionService)
        {
            this.promotionService = promotionService;
        }

        [HttpPost]
        public ApplyAsyncModel ApplyPromotionAsync([FromBody] ApplyPromotionRequest request)
        {
            return promotionService.ApplyAsync(new PlacePromotionModel
            {
                DealerId = request.DealerId,
                MSISDN = request.MSISDN,
                PromotionId = request.PromotionId
            });
        }

        [HttpPost]
        public ApplyModel ApplyPromotionSync([FromBody] ApplyPromotionRequest request)
        {
            return promotionService.Apply(new PlacePromotionModel
            {
                DealerId = request.DealerId,
                MSISDN = request.MSISDN,
                PromotionId = request.PromotionId
            });
        }

        [HttpGet]
        public ApplyModel GetStatus(int dealerId, int orderId)
        {
            return promotionService.GetStatus(new GetStatusModel
            {
                DealerId = dealerId,
                OrderId = orderId
            });
        }
    }
}
